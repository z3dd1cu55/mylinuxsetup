# MyLinuxSetup

### this Linux setup is inspired by Wes Doyle.

### install packages:
install: git; vim; (zsh; oh-my-zsh ??); tmux

install vundle (for vim) from git

### clone and setup repos

mkdir ~/repos

clone mylinuxsetup to /repos

cd mylinuxsetup

git config user.email "z3dd1cu55@..."

git config user.name "z3dd1cu55"

chmod +x .make.sh

run .make.sh

### install vim plugins

vim ~/.vimrc

:PluginInstall

